"""
mclib.py
    Utility routines and constants for the get_iplayer tool suite.
"""

import sys
import os
import pwd
import optparse
import traceback
import subprocess
import re


### Atributes
__author__ = "Alan Staniforth <alan@apollonia.org>"
__copyright__ = "Copyright (c) 2004-2010 Alan Staniforth"
__license__ = "New-style BSD"
# same format as sys.version_info: "A tuple containing the five components of
# the version number: major, minor, micro, releaselevel, and serial. All
# values except releaselevel are integers; the release level is 'alpha',
# 'beta', 'candidate', or 'final'. The version_info value corresponding to the
# Python version 2.0 is (2, 0, 0, 'final', 0)."  Additionally we use a
# releaselevel of 'dev' for unreleased under-development code.
version_info = (1, 0, 0, 'dev', 0)


#### Interface
__all__ = [ # Classes
            'PlainHelpFormatter',
            # Methods
            'version_info', 'getversionstr', 'is_root', 'get_cur_username',
            'sanitise_overwrite_files_list',
            # Constants
            'FAT_CELLAR_ROOT', 'SOURCE_ROOT', 'USER_FORK', 'BREW_FORK']


################################## Constants #################################
SYM_LINK_REGEX = r'(.*) -> (.*)$'
FAT_CELLAR_ROOT="/usr/local/FatCellar/"
SOURCE_ROOT="/usr/local/"
USER_FORK="user_files"
BREW_FORK="brew_files"

### Classes

### Class to improve formatting of optparse help output
# 
#   Helper class for optparse, tell your OptionParser to use this to format the help text when
#   first instantiate the object. Thus:
# 
#       parser = OptionParser(..., formatter=PlainHelpFormatter(), ...)
class PlainHelpFormatter(optparse.IndentedHelpFormatter): 
	def format_description(self, description):
		if description:
			return description + "\n"
		else:
			return ""

### Methods


def sanitise_overwrite_files_list(in_list):
	"""
	Cleans up explict symlink notation in files to be overwritten list
	
	"""
	out_list = []
	for file_to_check in in_list:
		parsed_name = re.search(SYM_LINK_REGEX, file_to_check)
		if parsed_name != None:
			out_list.append(parsed_name.group(1))
		else:
			out_list.append(file_to_check)
	return out_list

### getversionstr()
#	Returns:
#	   the library version number as a string
def getversionstr():
	vstring = '.'.join([
		str(version_info[0]),
		str(version_info[1]),
		str(version_info[2])
		])
	return vstring


### is_root()
# 
# Check if running as root
#
#   Returns:
#       'True' or 'False' as appropriate
def is_root():
    cur_user_name = get_cur_username()
    return cur_user_name == 'root'

### Get the current username
#   Returns:
#       The current user name as a Python string
def get_cur_username():
    return(pwd.getpwuid(os.getuid())[0])



